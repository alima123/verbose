package verbose

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type Verbose struct {
	err     *zap.SugaredLogger
	LogFile string
}

//var errorLogger *zap.SugaredLogger

var levelMap = map[string]zapcore.Level{
	"debug":  zapcore.DebugLevel,
	"info":   zapcore.InfoLevel,
	"warn":   zapcore.WarnLevel,
	"error":  zapcore.ErrorLevel,
	"dpanic": zapcore.DPanicLevel,
	"panic":  zapcore.PanicLevel,
	"fatal":  zapcore.FatalLevel,
}

func getLoggerLevel(lvl string) zapcore.Level {
	if level, ok := levelMap[lvl]; ok {
		return level
	}
	return zapcore.InfoLevel
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
	//return zapcore.NewJSONEncoder(encoderConfig)
}

func getLogWriter(logfile string) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   logfile,
		MaxSize:    1 << 30, //1G
		MaxBackups: 3,
		MaxAge:     30,
		Compress:   true,
	}
	return zapcore.AddSync(lumberJackLogger)
}

func (v *Verbose) Init() {
	level := getLoggerLevel("debug")
	core := zapcore.NewCore(getEncoder(), getLogWriter(v.LogFile), level)
	logger := zap.New(core, zap.AddCaller(), zap.AddCallerSkip(1))
	//errorLogger = logger.Sugar()
	v.err = logger.Sugar()
	//return logger.Sugar()
}

func (v *Verbose) Debug(args ...interface{}) {
	v.err.Debug(args...)
}

func (v *Verbose) Debugf(template string, args ...interface{}) {
	v.err.Debugf(template, args...)
}

func (v *Verbose) Info(args ...interface{}) {
	v.err.Info(args...)
}

func (v *Verbose) Infof(template string, args ...interface{}) {
	v.err.Infof(template, args...)
}

func (v *Verbose) Warn(args ...interface{}) {
	v.err.Warn(args...)
}

func (v *Verbose) Warnf(template string, args ...interface{}) {
	v.err.Warnf(template, args...)
}

func (v *Verbose) Error(args ...interface{}) {
	v.err.Error(args...)
}

func (v *Verbose) Errorf(template string, args ...interface{}) {
	v.err.Errorf(template, args...)
}

func (v *Verbose) DPanic(args ...interface{}) {
	v.err.DPanic(args...)
}

func (v *Verbose) DPanicf(template string, args ...interface{}) {
	v.err.DPanicf(template, args...)
}

func (v *Verbose) Panic(args ...interface{}) {
	v.err.Panic(args...)
}

func (v *Verbose) Panicf(template string, args ...interface{}) {
	v.err.Panicf(template, args...)
}

func (v *Verbose) Fatal(args ...interface{}) {
	v.err.Fatal(args...)
}

func (v *Verbose) Fatalf(template string, args ...interface{}) {
	v.err.Fatalf(template, args...)
}
