module bitbucket.org/alima123/verbose

go 1.13

require (
	bitbucket.org/alima123/util v0.0.0-20191226180549-8e7ab4020abd
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/sirupsen/logrus v1.5.0
	go.uber.org/zap v1.14.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
