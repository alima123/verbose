package verbose

import "testing"

func TestInfof2(t *testing.T) {
	verbose := &Verbose{LogFile: "test.log"}
	verbose.Init()
	verbose.Info("info")
	verbose.Infof("infof %v", "dsada")
	verbose.Debug("debug")
	verbose.Debugf("debugf %v", "ssss")
	verbose.Warn("warn")
	verbose.Warnf("warnf %v", "1111")
	verbose.Error("error")
	verbose.Errorf("errorf %v", "err")
	verbose.DPanic("dpanic")
	verbose.DPanicf("DPanicf %v", "ssss")
}
